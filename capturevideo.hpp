#ifndef CAPTUREVIDEO_H
#define CAPTUREVIDEO_H

#include <iostream>
#include "opencv.hpp"
#include "highgui/highgui.hpp"
#include "imgproc.hpp"
#include <iostream>
#include <fstream>
#include <cstdio>

#include "constants.hpp"
#include "tracking.hpp"
#include "cvops.hpp"


class CaptureVideo {

    private:

        cv::VideoCapture    camera;
        //cv::Mat             image;
        Tracking            tracking;

        struct {
            std::string username;
            std::string password;
            std::string ip;
            std::string port;
        } userData;

        std::fstream file;

    private:

        int connect();
        bool configFileExists();
        int readConfigFile();
        int createConfig();

    public:

        CaptureVideo();
        int init();
        void captureImage(cv::Mat &capture);

};

#endif // CAPTUREVIDEO_H
