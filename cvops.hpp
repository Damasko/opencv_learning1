#ifndef CVOPS_H
#define CVOPS_H

#include "opencv.hpp"

namespace CVOps {

    static inline void smooth1(cv::Mat &image) {
        //create structuring element that will be used to "dilate" and "erode" image.
        //the element chosen here is a 3px by 3px rectangle

        cv::Mat erodeElement = cv::getStructuringElement( cv::MORPH_RECT, cv::Size(3,3));
        //dilate with larger element so make sure object is nicely visible
        cv::Mat dilateElement = cv::getStructuringElement( cv::MORPH_RECT, cv::Size(8,8));

        cv::erode(image, image, erodeElement);
        cv::erode(image, image, erodeElement);


        cv::dilate(image, image, dilateElement);
        cv::dilate(image, image, dilateElement);

    }

};

#endif // CVOPS_H
