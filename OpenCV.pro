TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

CONFIG += link_pkgconfig
PKGCONFIG += opencv

INCLUDEPATH += /usr/include/opencv2 \

LIBS += -L /usr/lib -lopencv_core -lopencv_imgproc

SOURCES += main.cpp \
    capturevideo.cpp \
    tracking.cpp \
    program.cpp

HEADERS += \
    capturevideo.hpp \
    tracking.hpp \
    constants.hpp \
    program.hpp \
    cvops.hpp
