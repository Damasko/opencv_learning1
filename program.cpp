#include "program.hpp"

void on_trackbar(int , void*) {

}

Program::Program() {

    H_MIN = 0, H_MAX = 256;
    S_MIN = 0, S_MAX = 256;
    V_MIN = 0, V_MAX = 256;

}

Program::~Program() {

}

int Program::init() {

    if (camera.init() < 0) return 1;
    createTrackBars();
    return 0;

}

void Program::createTrackBars() {

    std::string trackbarWindowName = "Track Bars";
    cv::namedWindow(trackbarWindowName, cv::WINDOW_NORMAL);
    cv::createTrackbar( "H_MIN", trackbarWindowName, &H_MIN, H_MAX, on_trackbar );
    cv::createTrackbar( "H_MAX", trackbarWindowName, &H_MAX, H_MAX, on_trackbar );
    cv::createTrackbar( "S_MIN", trackbarWindowName, &S_MIN, S_MAX, on_trackbar );
    cv::createTrackbar( "S_MAX", trackbarWindowName, &S_MAX, S_MAX, on_trackbar );
    cv::createTrackbar( "V_MIN", trackbarWindowName, &V_MIN, V_MAX, on_trackbar );
    cv::createTrackbar( "V_MAX", trackbarWindowName, &V_MAX, V_MAX, on_trackbar );

}

int Program::run() {

    cv::Mat captured;
    cv::Mat HSV;
    cv::Mat threshold;
    bool running = true;
    int x=0, y=0;
    std::cout << "Programa en ejecucion, presiones Esc en el foco de alguna ventana para salir..." << std::endl;
    while (running) {

        camera.captureImage(captured);
        // Original con gaussian blur para reducir ruido
        //cv::GaussianBlur(captured, captured, cv::Size(3,3), 0, 0, cv::BORDER_DEFAULT );

        // HSV
        cv::cvtColor(captured, HSV, cv::COLOR_BGR2HSV);
        // Threshold
        cv::inRange(HSV,cv::Scalar(H_MIN,S_MIN,V_MIN), cv::Scalar(H_MAX,S_MAX,V_MAX), threshold);
        CVOps::smooth1(threshold);
        tracking.trackFilteredObject(x, y, threshold, captured);
        cv::imshow("Camera", captured);
        cv::imshow("Streaming", HSV);
        cv::imshow("Thresold", threshold);

        int key = cv::waitKey(1);
        if (key == 1048603) running = false;

    }

    captured.release();
    HSV.release();
    threshold.release();

    cv::destroyAllWindows();

    return 0;


}
