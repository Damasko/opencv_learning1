
#include "program.hpp"
#include <iostream>

using namespace std;
using namespace cv;

int main(int argc, char *argv[]) {

    Program program;
    if (program.init() < 0) return -1;

    return program.run();

}
