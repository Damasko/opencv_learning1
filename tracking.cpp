#include "tracking.hpp"

Tracking::Tracking() {

}

Tracking::~Tracking() {

}

void Tracking::trackFilteredObject(int &x, int &y, cv::Mat &threshold, cv::Mat &captured) {

    contours.clear();
    cv::Mat temp;
    std::vector<cv::Vec4i> hierarchy;
    threshold.copyTo(temp);
    cv::findContours(temp, contours, hierarchy, CV_RETR_CCOMP,CV_CHAIN_APPROX_SIMPLE );
    double refArea = 0;
    bool objectFound = false;
    if (hierarchy.size() > 0) {
        int numObjects = hierarchy.size();
        if(numObjects<MAX_NUM_OBJECTS){
            for (int index = 0; index >= 0; index = hierarchy[index][0]) {

                cv::Moments moment = moments((cv::Mat)contours[index]);
                double area = moment.m00;

                //if the area is less than 20 px by 20px then it is probably just noise
                //if the area is the same as the 3/2 of the image size, probably just a bad filter
                //we only want the object with the largest area so we safe a reference area each
                //iteration and compare it to the area in the next iteration.
                if(area>MIN_OBJECT_AREA && area<MAX_OBJECT_AREA && area>refArea){
                    x = moment.m10/area;
                    y = moment.m01/area;
                    objectFound = true;
                    refArea = area;
                }
                else objectFound = false;


            }
            //let user know you found an object
            if(objectFound ==true){
                putText(captured,"Tracking Object", cv::Point(0,50), 2, 1, cv::Scalar(0,255,0),2);
                //draw object location on screen
                drawObject(x, y, captured);
            }

        }
        else cv::putText(captured,"TOO MUCH NOISE! ADJUST FILTER", cv::Point(0,50),1,2, cv::Scalar(0,0,255),2);
    }

}

void Tracking::drawObject(int x, int y, cv::Mat &frame) {

    cv::circle(frame, cv::Point(x,y), 20, cv::Scalar(0,255,0), 2);
    if(y-25>0) cv::line(frame, cv::Point(x,y), cv::Point(x,y-25), cv::Scalar(0,255,0),2);
    else cv::line(frame, cv::Point(x,y), cv::Point(x,0), cv::Scalar(0,255,0),2);
    if(y+25<FRAME_HEIGHT) cv::line(frame, cv::Point(x,y), cv::Point(x,y+25), cv::Scalar(0,255,0),2);
    else cv::line(frame, cv::Point(x,y), cv::Point(x,FRAME_HEIGHT), cv::Scalar(0,255,0),2);
    if(x-25>0) cv::line(frame, cv::Point(x,y), cv::Point(x-25,y), cv::Scalar(0,255,0),2);
    else cv::line(frame, cv::Point(x,y), cv::Point(0,y), cv::Scalar(0,255,0),2);
    if(x+25<FRAME_WIDTH) cv::line(frame, cv::Point(x,y), cv::Point(x+25,y), cv::Scalar(0,255,0),2);
    else cv::line(frame, cv::Point(x,y), cv::Point(FRAME_WIDTH,y), cv::Scalar(0,255,0),2);

    cv::putText(frame, std::to_string(x)+","+ std::to_string(y), cv::Point(x, y+30), 1, 1, cv::Scalar(0,255,0), 2);


}
