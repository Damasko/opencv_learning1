#include "capturevideo.hpp"


CaptureVideo::CaptureVideo() {

    userData.username   = "";
    userData.password   = "";
    userData.ip         = "";
    userData.port       = "";

}


int CaptureVideo::init() {

    if (connect() < 0) return -1;
    return 0;
}

bool CaptureVideo::configFileExists() {

    bool exist = true;
    file.open("data.txt", std::fstream::in | std::fstream::out);
    if (!file) {
        exist = false;

    }
    file.close();
    return exist;

}

int CaptureVideo::readConfigFile() {

    std::ifstream inputFile("data.txt");
    std::string fileData1, fileData2;
    while (inputFile >> fileData1 >> fileData2) {
        if (fileData1 == "user") userData.username = fileData2;
        else if (fileData1 == "password") userData.password = fileData2;
        else if (fileData1 == "ip") userData.ip = fileData2;
        else if (fileData1 == "port" ) userData.port = fileData2;

        else {
            std::cout << "Error al parsear el fichero de configuracion: Opcion no reconocida (" << fileData1 << ")" << std::endl;
            std::cout << "El archivo contiene errores y sera eliminado" << std::endl;
            inputFile.close();
            if (std::remove("data.txt") < 0) {
                std::cout << "Archivo de configuracion eliminado" << std::endl;
                return -1;
            }
        }

    }
    inputFile.close();

}

int CaptureVideo::createConfig() {

    file.open("data.txt", std::fstream::in | std::fstream::out | std::fstream::trunc);
    char response;
    std::cout << "La camara tiene nombre de usuario? (s/n)" << std::endl;
    std::cin >> response;
    if (response == 's' or response == 'S' ) {
        std::cout << "Indique nombre de usuario" << std::endl;
        std::cin >> userData.username;
        file << "user " << userData.username << std::endl;
    }
    std::cout << "La camara tiene contraseña? (s/n)" << std::endl;
    std::cin >> response;
    if (response == 's' or response == 'S' ) {
        std::cout << "Indique contraseña" << std::endl;
        std::cin >> userData.password;
        file << "password " << userData.password << std::endl;
    }
    std::cout << "Indique IP del telefono" << std::endl;
    std::cin >> userData.ip;
    std::cout << "Puerto del servicio IP Webcamera" << std::endl;
    std::cin >> userData.port;

    file << "ip " << userData.ip << std::endl;
    file << "port " << userData.port;
    std::cout << "Fichero de configuracion creado" << std::endl;
    file.close();

}

int CaptureVideo::connect() {

    char response;
    if (configFileExists()) {
        std::cout << "Fichero de configuracion encontrado, desea usar este fichero? (s/n) " << std::endl;
        std::cin >> response;
    }

    if (response == 's' or response == 'S' ) {
        readConfigFile();

    }
    else {
        createConfig();
    }

    std::string finalURL;
    if (userData.username != "") finalURL = "http://" + userData.username + ":" + userData.password + "@" + userData.ip + ":" + userData.port + "/video";
    else finalURL = "http://" + userData.ip + ":" + userData.port + "/video";
    //finalURL = "http://91.183.206.239:82/mjpg/video.mjpg"; // focas
    camera.open(finalURL);
    camera.set(CV_CAP_PROP_FRAME_WIDTH, FRAME_WIDTH);
    camera.set(CV_CAP_PROP_FRAME_HEIGHT, FRAME_HEIGHT);
    if(!camera.isOpened()) {

        std::cout << "Error al crear el streaming" << std::endl;
        return -1;

    }
    std::cout << "Conectado con la camara remota." << std::endl;
    return 0;
}

void CaptureVideo::captureImage(cv::Mat &capture) {

    if (!camera.read(capture)) {
        std::cout << "No frame avaible" << std::endl;
    }

}


