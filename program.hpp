#ifndef PROGRAM_H
#define PROGRAM_H

#include "opencv.hpp"
#include "capturevideo.hpp"
#include "tracking.hpp"
#include "constants.hpp"

class Program {

    public:

        int H_MIN;
        int H_MAX;
        int S_MIN;
        int S_MAX;
        int V_MIN;
        int V_MAX;

    private:

        CaptureVideo    camera;
        Tracking        tracking;

    private:

        void createTrackBars();

    public:

        Program();
        ~Program();
        int init();
        int run();

};

#endif // PROGRAM_H
