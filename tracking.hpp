#ifndef TRACKING_H
#define TRACKING_H

#include "opencv.hpp"
#include "constants.hpp"
#include <vector>

class Tracking {

    public:
        static const int MAX_NUM_OBJECTS = 40;
        static const int MIN_OBJECT_AREA = 20*20;
        static const int MAX_OBJECT_AREA = FRAME_HEIGHT*FRAME_WIDTH/1.5;
        std::vector<std::vector<cv::Point> > contours;
        cv::Mat HSV;

    public:
        Tracking();
        ~Tracking();
        void trackFilteredObject(int &x, int &y, cv::Mat &threshold, cv::Mat &captured);
        void drawObject(int x, int y, cv::Mat &frame);
};

#endif // TRACKING_H
